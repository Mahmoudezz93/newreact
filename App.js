import React ,{useState,useEffect} from 'react';
import AppLoading from 'expo-app-loading'
import { Ionicons,MaterialCommunityIcons,MaterialIcons,Entypo,FontAwesome } from '@expo/vector-icons';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import * as Font from 'expo-font';
import 'react-native-gesture-handler';
import AppContainer from './navigation/AppContainer';
import { NavigationContainer } from '@react-navigation/native';

import { Button, View ,Container,Text,Content,Header,StyleProvider} from 'native-base';
 
 
import {StatusBar} from"react-native";
import { Platform ,Dimensions} from 'react-native';
const platform = Platform.OS;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;


const FetchFonts = async () =>
{
  await Font.loadAsync({
    Roboto: require('native-base/Fonts/Roboto.ttf'),
    Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    ...MaterialCommunityIcons.font,
    ...MaterialIcons.font,
    ...Entypo.font,
    ...FontAwesome.font
  });
  

}

export default function App()   {
  
   
  const [fontLoaded,setFontLoaded] = useState(false) 
  let [isReady, setIsReady] = React.useState(false)

  useEffect(()=>
  {
    console.log('i will fetch the fonts ')
      FetchFonts
      console.log('Done and now lets go to the app ')
      setIsReady(true)
       
  },[])
  
    

    return (
      isReady ? 
      <StyleProvider style={getTheme(material)}>
      <Container style={platform=="ios"?{paddingTop:deviceHeight/26 ,backgroundColor:"#000"}:{backgroundColor:"#000"}  }> 
      <StatusBar barStyle="dark-content" style={{height:0 ,padding:0}}/>
       <AppContainer/>
       </Container>
      </StyleProvider>
        : <AppLoading/>
    );
  
}