import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Login from '../screens/auth/login';
import Welcome from '../screens/ecommerce/welcome'
 
const MainStack = createStackNavigator();

const AppContainer = props => {
   
    return (
      <NavigationContainer>
        <MainStack.Navigator>
          <MainStack.Screen name="Welcome" component={Welcome} />
        </MainStack.Navigator>
      </NavigationContainer>
    );
  };
  
  export default AppContainer;
  